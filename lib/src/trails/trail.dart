/// Base trail model (used in list)
class BaseTrail {
  final int id;
  final String name;
  final String start;

  const BaseTrail({
    required this.id,
    required this.name,
    required this.start,
  });
}

/// Full trail model
class Trail extends BaseTrail {
  final String end;
  final int duration;
  final int distance;
  final int positiveDrop;
  final int negativeDrop;
  final String trackData;

  const Trail({
    required super.id,
    required super.name,
    required super.start,
    required this.end,
    required this.duration,
    required this.distance,
    required this.positiveDrop,
    required this.negativeDrop,
    required this.trackData,
  });
}
