import 'package:flutter/material.dart';

/// Displays detailed information about a trail.
class TrailDetailView extends StatelessWidget {
  const TrailDetailView({super.key});

  static const routeName = '/trail';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Trail details'),
      ),
      body: const Center(
        child: Text('More Information Here'),
      ),
    );
  }
}
